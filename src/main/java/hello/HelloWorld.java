package hello;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.LongAccumulator;
import java.util.function.LongBinaryOperator;
import java.util.stream.IntStream;

public class HelloWorld {
    public static void main(String[] args) throws Exception {

        HelloWorld hello = new HelloWorld();

        hello.go1();
        // hello.go2();
    }

    public void go1() throws Exception{
        System.out.println("go1");
    }

    public void go2() throws InterruptedException{
        //        LocalTime currentTime = new LocalTime();
//        System.out.println("The current local time is: " + currentTime);

        // 單一執行緒 看結果 salary.txt
        Salary salary = new Salary();
        // salary.showSalary();


        // 開了三個執行緒 看結果 salary.txt
        // Thread first = new Thread(new Salary());
        // Thread second = new Thread(new Salary());
        // Thread third = new Thread(new Salary());

        // first.start();
        // second.start();
        // third.start();

        //重新來過
        //透過produce and consumer 來進行
        Thread proThread = new Thread(new Runnable(){
            @Override
            public void run(){
                try{
                    salary.produce();
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }
        });

        Thread conThread = new Thread(new Runnable(){
            @Override
            public void run(){
                try{
                    salary.consume();
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }
        });

        Thread conThread2 = new Thread(new Runnable(){
            @Override
            public void run(){
                try{
                    salary.consume();
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }
        });
        
        proThread.start();
        conThread.start();
        conThread2.start();

        proThread.join();
        conThread.join();
    }

    public void go3(){
        LongBinaryOperator op = (x, y) -> 2 * x + y;
        LongAccumulator accumulator = new LongAccumulator(op, 10L);

        ExecutorService executor = Executors.newFixedThreadPool(2);

        IntStream.range(0, 10)
            .forEach(i -> executor.submit(() -> accumulator.accumulate(i)));

        // stop(executor);
        
        System.out.println(accumulator.getThenReset());     // => 2539

          // executor.shutdown();
    }
}
