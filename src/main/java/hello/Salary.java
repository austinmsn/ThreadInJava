package hello;

import java.io.*;
import java.util.LinkedList;
import java.util.Properties;

public class Salary {

    LinkedList<File> fileList = new LinkedList<>();
    int capacity = 100;

    public void produce() throws InterruptedException {
        String folderPath = "/c/git/ThreadInJava/src/main/resources";
        int count = 0;
        File[] files = listFiles(folderPath);
        while (true) {
            synchronized (this) {
                while (fileList.size() == capacity)
                    wait();
                System.out.println("count:" + count);
                if (files[count].exists()) {
                    System.out.println("Producer produced-" + files[count].getName());
                    fileList.add(files[count]);
                    count++;
                } else {
                    System.out.println("files[" + count + "] not exists.");
                }

                notify();
                Thread.sleep(30);
            }
        }
    }

    public void consume() throws InterruptedException, IOException {
        String outPath = "/c/git/ThreadInJava/salary.txt";
        Properties prop = new Properties();
        while (true) {
            synchronized (this) {
                while (fileList.size() == 0)
                    wait();
                File value = fileList.removeFirst();
                Properties src = new Properties();
                src.load(new FileInputStream(value));
                System.out.println("Consumer consumed-" + value.getName());
                Properties newProp = trans(prop, src);
                newProp.save(new FileOutputStream(outPath), "comments");
                notify();

                Thread.sleep(100);
            }
        }
    }


    /**
     * @param File       file properties file
     * @param Properties src Properties file
     * @throws IOException
     * @throws FileNotFoundException summary all files
     */
    public Properties trans(Properties prop, Properties src) throws FileNotFoundException, IOException {

        if (src == null) return prop;

        for (Object key : src.keySet()) {
            Object valueSrc = src.get(key);
            if (prop.keySet().contains(key)) {
                Object valueFile = prop.get(key);
                Object valueSum = Double.valueOf(valueFile.toString()) + Double.valueOf(valueSrc.toString());
                prop.setProperty(key.toString(), valueSum.toString());
            } else {
                prop.setProperty(key.toString(), valueSrc.toString());
            }
        }
        return prop;
    }

    /**
     * list all files
     */
    public File[] listFiles(String folderPath) {

        File folder = new File(folderPath);
        if (!folder.isDirectory()) {
            return null;
        }
        File[] listOfFiles = folder.listFiles();

        // System.out.println(Thread.currentThread()+"listFiles");
        System.out.println(Thread.currentThread() + folder.getPath());

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                // System.out.println(Thread.currentThread()+"File " + listOfFiles[i].getName());
            } else if (listOfFiles[i].isDirectory()) {
                // System.out.println(Thread.currentThread()+"Directory " + listOfFiles[i].getName());
            }
        }

        return listOfFiles;
    }
}